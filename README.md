# Vfio-vm-tools

## TLDR

Use this script for launching qemu/KVM user session virtual machines to improve their performance with CPU pinning and isolation.

Basic usage:
`vfio-vm-tools --operation (start/stop) --name (VM_NAME)`

## Description

Vfio-vm-tools is a simple bash script for managing virtual machines running in qemu/KVM user session. It picks up the cpu core pinning and isolates the virtual machines from each other and the system.

This script requires systemd. When using it without configuration file, the user system must provide binaries:

- tee
- cut
- tr
- grep
- tail

If you want to use the script with the configuration file, the tool additionally requires `jq`.

## Usage

The tool is intended to be a complete solution for managing the state of the usermode VMs with CPU pinning.

### Installation

To install the script you must first clone or download this repository to your machine. To allow the script to be executed from any location and without specifying the absolute path to the script you can either copy it to the distribution binary location (for example `/usr/local/bin/`) or add the directory to system path. Please consult your distribution documentation for instructions on recommended steps. Please also make sure that the file ownership is correctly set so that other users can not add arbitrary commands to be executed as root (for example - user: root, owner: root, file permissions: rwxr-xr-x / 755).

The following sections assume that the tool is added to the system path.

### Basic use

Basic use of the tool requires executing the script and passing VM name and operation to be performed. List of arguments:
- `--vm-name` or `-n` - the name (or alias - see the Configuration file section below) of the virtual machine to be managed
- `--operation` or `-o` - the operation name to be performed. Available options are:

	- start - starts the virtual machine
	- stop - stops the virtual machine
	- status - displays the virtual machine status
	- unlock - unisolates all the processes - restores all available cores to host

### Configuration file

Configuration file can be placed in the default location: `~/.config/vfio-vm-tools/config.json`, or the config location can be passed to the script with the `--config / -c` argument.

For now the configuration file allows the user to define aliases for each virtual machine and select the cores on which the user can operate.

Defining aliases can ease the burden of remembering the complex vm names. Instead of typing `vfio-vm-tools -n 01_ubuntu_22.04_cluster1 -o start` you can simply alias the vm name and use something more friendly, such as `vfio-vm-tools -n kube_cluster1 -o start`.

During the VM starts and cleanups the script temporarily unisolates the cores. This can have unwanted consequences, such as allowing the user processes to run on cores that are reserved for a VM running in system session. The config file allows the user to limit the cores that are unlocked during operation.

#### Example configuration file

Below json snippet contains all the fields that must be present in the configuration file. In the `vmConfig` section the user can define as many VM aliases as they wish.

```json
{
	"globalConfig": {
		"__comment": "Specify a list of cores that are available to you as a user",
		"availableCores": "0-7"
	},
	"vmConfig": {
		"work_linux": {
			"__comment": "This VM will be started with core isolation enabled",
			"libvirtName": "my_very_special_dev_vm",
			"isolateCores": true
		},
		"work_windows": {
			"__comment": "This VM will be started with core isolation disabled",
			"libvirtName": "complex_name_of_windows_vm",
			"isolateCores": false
		},
		"work_partial_config": {
			"__comment": "This VM has only name specified, the isolation is controlled with script",
			"libvirtName": "some_other_name"
		}
	}	
}

```

You can find this example configuration in [the doc directory of this repo](doc/config.json).

### Virtual machine configuration

To properly utilize CPU pinning and VM isolation one must properly configure the VM. For details please consult the arch wiki entry on [cpu pinning](https://wiki.archlinux.org/title/PCI_passthrough_via_OVMF#CPU_pinning).

## Background

### Why use isolation?

When setting up a stock VM using libvirt the cores of the VM will be represented on the host system as a set of processes. If we create a VM with 4 cpu cores available, the host system will create 4 processes. Linux kernel will then treat those VM cpu processes as usual processes, meaning it will change the core on which they are executed. This can result in suboptimal performance, since during some such migrations the processor cache is lost.

To remedy this we can statically assign the VM cores so that the processes corresponding to the VM cores are always executed on the same physical CPU core. This saves us from the cache invalidation from the process jumping around the cores, but host kernel can still assign processes to run on those cores. This is another opportunity for performance loss, since the cache can be invalidated and also the VM cores can also be hogged by other applications.

To prevent this we can use a process called core isolation. In this process we can utilize a kernel mechanism called cgroups. This boils down to creating a new process group, assigning this group to be executed on the particular CPU cores and preventing other processes from executing on the VM cores.

If you wish to learn more I highly recommed reading excellent arch wiki entries on [cpu pinning](https://wiki.archlinux.org/title/PCI_passthrough_via_OVMF#CPU_pinning) and [cpu isolation](https://wiki.archlinux.org/title/PCI_passthrough_via_OVMF#Isolating_pinned_CPUs).

### Why is this tool needed

We can manually control this, for example using systemd or other tools, but it is boring and tedious. To properly isolate a vm running in user session, you must first:

- create cgroup for the VM
- check on which cores the VM should be able to run
- run the VM
- get the PID of the freshly started virtual machine
- allow the freshly created cgroup to run on the cores specified in the VM config
- move the VM process to the cgroup created for it
- limit user and system processes just to other cores

The situation becomes even worse when you want to start more than one VM. When limiting user and system processes you must know the cores that are used by all the running VMs. It quickly becomes usability nightmare. To ease the process I created this tool.

### Why not use vfio-isolate?

There already is a project that aims to ease the pains of setting up core isolation task, called [vfio-isolate](https://github.com/spheenik/vfio-isolate). This is a cool tool, but I have a few issues with it.

1. I  want a tool that is simple to use. Vfio-isolate requires the user to carefully copy the core assignment from the libvirt configuration to the qemu hook. It is easy to forget to update the script when updating the VM configuration. Vfio-vm-tools automatically collects all the pinning information of the running VMs and allows the user to not worry about the scripts.
2. Vfio-isolate is designed to be used with qemu hook. Since I am almost exclusively using virtual machines in qemu/KVM user session, starting hooks as root is rather problematic.
3. I also want a tool that is standalone and that allows the user to start and stop VMs.
4. Vfio-isolate is written in python, which is fine and I like the language. However, for this task I wanted something that is as simple as possible.
5. I wanted to make my own tool :)

### Practical demonstration

First, let's take a look at the output of htop on example system:

![System in idle](doc/01_base_idle.png "System in idle")

Then let's load all the cores using dd:

```bash
dd if=/dev/zero of=/dev/null | dd if=/dev/zero of=/dev/null | dd if=/dev/zero of=/dev/null | dd if=/dev/zero of=/dev/null | dd if=/dev/zero of=/dev/null | dd if=/dev/zero of=/dev/null | dd if=/dev/zero of=/dev/null | dd if=/dev/zero of=/dev/null | dd if=/dev/zero of=/dev/null | dd if=/dev/zero of=/dev/null | dd if=/dev/zero of=/dev/null | dd if=/dev/zero of=/dev/null | dd if=/dev/zero of=/dev/null | dd if=/dev/zero of=/dev/null | dd if=/dev/zero of=/dev/null | dd if=/dev/zero of=/dev/null

```

This will start 16 dd processes, fully loading a CPU. This is demonstrated below:

![Fully loaded system](doc/02_dd_load.png "Fully loaded system")

Now if we wanted to start a VM pinned to cores 12-15, the host system will be able to also run on this cores, potentially causing VM performance issues. If we start the VM and isolate it from the host, the guest VM cores will be fully available to guest. Consider the next screenshot - here I started the VM with properly configured VM that is set to run on cores 12-15. Note that there is some load on core 13 - but this comes from the guest VM.

![Fully loaded system with isolated VM](doc/03_vm_started.png "Fully loaded system with isolated VM")

### Benchmarking the test vm

To judge the impact of the cpu pinning and isolation I setup the ubuntu 22.04 VM with 4 cores and 8 threades assigned to it. This is a stock minimal install updated to the latest version of packages. To fully control the process execution, both the host and the guest were disconnected from the network. On the host, 4 dd processes were started to simulate the load from processes other than our VM. The guest performance was measured using `sysbench cpu run --threads=8`, and timed linux kernel compile. Before each test the VM was restored to the same snapshot. The guest is running llvmpipe, so the glxgears render results are based on the guest CPU performance.

Besides acpi_cpufreq also amd_pstate was tested, but the results were mixed. For loaded system the compilation time with schedutil and isolated vm increased from 1m13 - 1m20 to 1m15 - 1m27. So it seems that for tested workload is not really beneficial.

#### Schedutil - acpi_cpufreq

| VM configuration                                          | Sysbench Events per second | Sysbench Latency avg | Sysbench Latency max | Kernel compilation time | glxgears fps |
|-----------------------------------------------------------|----------------------------|----------------------|----------------------|-------------------------|--------------|
| No pinning - idle host                                    | 41354.03                   | 0.19                 | 20.73                | 0m54,217s               | 2468         |
| CPUs pinned to sequential cores (bad pinning) - idle host | 23511.26                   | 0.34                 | 19.28                | 1m24,999s               | 2389         |
| CPUs pinned to sibling cores (good pinning)  - idle host  | 23480.20                   | 0.34                 | 16.98                | 1m12,965s               | 2337         |
| CPUs pinned and isolated   - idle host                    | 41294.99                   | 0.19                 | 18.94                | 0m52,985s               | 2442         |
| No pinning                                                | 32596.84                   | 0.24                 | 20.30                | 1m40,205s               | 1914         |
| CPUs pinned to sequential cores (bad pinning)             | 22698.33                   | 0.35                 | 24.54                | 1m35,712s               | 968          |
| CPUs pinned to sibling cores (good pinning)               | 22687.14                   | 0.35                 | 30.68                | 1m51,677s               | 882          |
| CPUs pinned and isolated                                  | 22960.23                   | 0.34                 | 24.78                | 1m20,337s               | 2341         |

#### Performance - acpi_cpufreq

With this governor, only 4 cases were tested. The general system performance was a bit mixed, since as the cpu got warmer the tests seemed to take a hit in performance, especially kernel compilation times. The care was taken to ensure that the system was warmed up before testing, but it was just a quick test so the test methodology was not perfect.

| VM configuration                                          | Sysbench Events per second | Sysbench Latency avg | Sysbench Latency max | Kernel compilation time | glxgears fps |
|-----------------------------------------------------------|----------------------------|----------------------|----------------------|-------------------------|--------------|
| No pinning - idle host                                    | 41414.90                   | 0.19                 | 13.87                | 1m17,938s               | 2484         |
| CPUs pinned and isolated   - idle host                    | 41189.53                   | 0.19                 | 15.10                | 1m3,251s                | 2527         |
| No pinning                                                | 30502.38                   | 0.26                 | 40.19                | 1m24,279s               | 1914         |
| CPUs pinned and isolated                                  | 22953.29                   | 0.34                 | 31.99                | 1m26,115s               | 2262         |

#### Benchmark - summary

The results from the table are clear. Pinning and isolating the cpu cores usually gives a pretty significant performance boost. In practical use it mostly reduces the stuttering, since there are many factors that impact the final scores of the benchmarks.
