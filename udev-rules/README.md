# How to use udev rules

The set of files in this directory is used to set the permissions of the devices for the respective users. In order to use them on your host system, copy the files to the `/etc/udev/rules.d/` directory. For maximum safety, change the owner and the group to the root user and assign the 500 permissions to those files.
