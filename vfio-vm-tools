#!/bin/bash

# ---------------------------------------------------------------------------------------
# | Global variables controlling the script execution                                   |
# ---------------------------------------------------------------------------------------
HELP=0
VM_NAME_TOP=0
OPERATION=0
CONFIG=0
CORE_ISOLATE_RUNTIME=true

# ---------------------------------------------------------------------------------------
# | Parse the script options                                                            |
# ---------------------------------------------------------------------------------------
SHORT=-h,n:,o:,c:,i:
LONG=help,vm-name:,operation:,config:,isolate:

PARSED_OPTIONS=$(getopt --options "$SHORT" --longoptions "$LONG" --name "$0" -- "$@")
STATUS=$?
if [ "$STATUS" != 0 ]; then
	exit 2
fi

eval set -- "$PARSED_OPTIONS"
while [[ $# -gt 0 ]]; do
    case "$1" in
        -h | --help) HELP=1;;
        -n | --vm-name) shift 
        				VM_NAME_TOP="$1";;
        -o | --operation) shift 
        				OPERATION="$1";;
        -c | --config) shift
        				CONFIG="$1";;
        -i | --isolate) shift
        				CORE_ISOLATE_RUNTIME="$1";;
    esac
    shift
done
set -- "${POSITIONAL[@]}"

# ---------------------------------------------------------------------------------------
# | Define all the tool functions                                                       |
# ---------------------------------------------------------------------------------------
script_help(){
echo "
VFIO VM helper script
	
This tool helps you manage and isolate your QEMU virtual machines running in user session.

Mandatory arguments:
--vm-name (-n) - virtual machine name as specified in the configuration file
--operation (-o) - operation to be performed. Possible options: start, stop, status, 
		lock, unlock

Optional arguments:
--config (-c) - specify the path to the configuration file. If not defined, the script
		runs with default options and requires the full virtual machine name to be 
		passed as an argument
--isolate (-i) - specify if the CPU cores are to be pinned after VM startup. This option
		is used together with the VM configuration from the config file. If the config 
		does not permit the CPU isolation, then this option does nothing.
		
In case of any issues please see the project website:
https://gitlab.com/krokodyl1220/vfio-vm-tools
"
}

# ---------------------------------------------------------------------------------------
# | Run the script logic                                                                |
# ---------------------------------------------------------------------------------------
run_script_logic(){
	if [ "$HELP" != 1 ]; then
		if [ "$OPERATION" == 0 ]; then
			echo "Error: No operation specified" 1>&2
			echo 2
			
		elif [ "$OPERATION" != "start" ] && [ "$OPERATION" != "stop" ] && 
			[ "$OPERATION" != "status" ] && [ "$OPERATION" != "unlock" ] &&
			[ "$OPERATION" != "lock" ]; then
			echo "Error: Wrong operation specified" 1>&2
			echo 3

		elif [ "$VM_NAME_TOP" == 0 ]; then
			if [ "$OPERATION" != "lock" ] && [ "$OPERATION" != "unlock" ]; then
				echo "Error: No vm name specified" 1>&2
				echo 4
			fi
		else
			echo 0
		fi
	else
		echo 1	
	fi
}

STATUS=$(run_script_logic)
if [ "$STATUS" == 1 ]; then
	script_help
	exit 0
elif [ "$STATUS" != 1 ] && [ "$STATUS" != 0 ]; then
	script_help
	exit "$STATUS"
fi

# ---------------------------------------------------------------------------------------
# | Create a temp directory that will hold the current vm states, pids and other info   |
# ---------------------------------------------------------------------------------------
TEMP_DIR="/tmp/vfio-vm-tools"
mkdir -p $TEMP_DIR

import_config(){
	GLOBAL_CORE_LIST=$(jq -r '.globalConfig.availableCores' < "$CONFIG")
	VM_LIST=$(jq -r '.vmConfig | keys | .[]' < "$CONFIG")
}

default_config(){
	GLOBAL_CORE_LIST=$(lscpu | grep 'On-line CPU' | cut -d' ' -f16)
	VM_LIST=$(virsh list --all | tail -n +3 | cut -d' ' -f6)
}

# ---------------------------------------------------------------------------------------
# | Get the basic information about the system. If the config file is present, use the  |
# | values from the configuration file. Otherwise use the defaults.                     |
# ---------------------------------------------------------------------------------------
if [ "$CONFIG" == 0 ]; then
	if [ -f ~/.config/vfio-vm-tools/config.json ]; then
		CONFIG=~/.config/vfio-vm-tools/config.json
		echo "Info: Using configuration file: $CONFIG"
		import_config
	else	
		CONFIG="DEFAULTS"
		echo "Info: No config specified and none present, running with defaults"
		default_config
	fi
else
	echo "Info: Using configuration file: $CONFIG"
	if [ -f $CONFIG ]; then
		import_config
	else
		echo "Error: Specified config file $CONFIG does not exist"
		exit 5
	fi
fi

# ---------------------------------------------------------------------------------------
# | Below functions are all dealing with the VM core assignments. The information about |
# | core pinning is derived directly from the VM libvirt config.                        |
# ---------------------------------------------------------------------------------------

# ---------------------------------------------------------------------------------------
# | Get the list of pinned cores for the specific VM name                               |
# ---------------------------------------------------------------------------------------
vm_get_pinned_cores(){
	NAME=$1
	CORELIST_RAW=$(virsh dumpxml "$NAME" | grep vcpupin | cut -d'=' -f3 | cut -d"'" -f2)
	CORELIST=""
	
	for ITEM in $CORELIST_RAW; do
		if [ "$CORELIST" == "" ]; then
			CORELIST=${ITEM}
		else
			CORELIST="$CORELIST,$ITEM"
		fi
	done
	echo "$CORELIST"
}

# ---------------------------------------------------------------------------------------
# | Get the name of the virtual machine if using the configuration file, otherwise      |
# | return the VM name                                                                  |
# ---------------------------------------------------------------------------------------
vm_get_name(){
	NAME=$1

	if [ "$CONFIG" == "DEFAULTS" ]; then
		echo "$NAME"	
	else	
		for ITEM in $VM_LIST; do
			if [ "$ITEM" == "$NAME" ]; then
					STRING=".vmConfig.$NAME.libvirtName"
					LIBVIRT_NAME=$(jq -r "$STRING" < "$CONFIG")
				break;
			fi	
		done

		echo "$LIBVIRT_NAME"
	fi
}

# ---------------------------------------------------------------------------------------
# | Check if the VM with the name specified is present in the configuration file. If    |
# | disabled, do not allow isolation, and if the parameter is not defined in the config |
# | core isolation is enabled and will be controlled by the script command line param   |
# ---------------------------------------------------------------------------------------
vm_get_isolation(){
	NAME=$1
	if [ "$CONFIG" == "DEFAULTS" ]; then
		echo "true"	
	else	
		for ITEM in $VM_LIST; do
			if [ "$ITEM" == "$NAME" ]; then
					STRING=".vmConfig.$NAME.isolateCores"
					ISOLATE_ENABLE=$(jq -r "$STRING" < "$CONFIG")

					# If the config has no specific instructions for 
					# isolation, allow the isolation
					if [ "$ISOLATE_ENABLE" == "null" ]; then
						ISOLATE_ENABLE="true"
					fi
				break;
			fi	
		done

		echo "$ISOLATE_ENABLE"
	fi
}

# ---------------------------------------------------------------------------------------
# | Check if the specified VM is running. This function uses the vm_get_name to get the |
# | libvirt name.                                                                       |
# ---------------------------------------------------------------------------------------
is_vm_running(){
	NAME=$1

	(virsh list | grep "$NAME") > /dev/null 2>&1
	STATUS=$?
	
	if [ "$STATUS" == 0 ]; then
		echo "running"
	else
		echo "shutdown"
	fi	
}

# ---------------------------------------------------------------------------------------
# | Check if the core list is supplied as a list or as ranges. If it is supplied as     |
# | ranges then walk through the ranges and construct the list of cores	                |
# ---------------------------------------------------------------------------------------
unfold_core_list(){
	CORES=$1
	
	# Check if the supplied core list has any ranges in it
	echo "$CORES" | grep '-' > /dev/null 2>&1
	STATUS=$?

	if [ $STATUS == 0 ]; then
	
		NEWCORES=""
		TEMP_CORES=${CORES//,/ }
			
		for ITEM in $TEMP_CORES; do
		
			START=$(echo "$ITEM" | cut -d'-' -f1)	
			END=$(echo "$ITEM" | cut -d'-' -f2)		
			
			for (( i=START; i<=END; i++ )); do 
				if [ "$NEWCORES" == "" ]; then
					NEWCORES="$i"
				else
					NEWCORES="$NEWCORES,$i"
				fi				
			done
		done
		echo $NEWCORES
	else
		# If no ranges are supplied, just return the original list
		echo "$CORES"
	fi	
}

# ---------------------------------------------------------------------------------------
# | Create a cgroup for a virtual machine under the current user's group.               |
# ---------------------------------------------------------------------------------------
create_cgroup(){
	NAME=$1
	MY_UID="$UID"
	
	set -xe	
	sudo mkdir -p /sys/fs/cgroup/user.slice/user-"$MY_UID"-"$NAME".slice
	set +xe
}

# ---------------------------------------------------------------------------------------
# | Assign a PID of the virtual machine to the appropriate cgroup that was constructed	|
# | specifically for this process                                                       |
# ---------------------------------------------------------------------------------------
move_pid_to_cgroup(){
	NAME=$1
	PID=$2
	MY_UID="$UID"
	
	set -xe
	echo "$PID" | sudo tee /sys/fs/cgroup/user.slice/user-"$MY_UID"-"$NAME".slice/cgroup.procs
	set +xe
}

cleanup_virsh_list(){
	NAME=$1

	# Remove the first set of double commas
	NAME=${NAME//',,'/','}
	
	# Are there any remains of double comma?
	echo "$NAME" | grep ',,' > /dev/null 2>&1
	STATUS=$?
	
	# Recursively remove the last double commas until none are left
	if [ $STATUS != 0 ]; then
		echo "$NAME"
	else	
		cleanup_virsh_list "$NAME"
	fi
}

# ---------------------------------------------------------------------------------------
# | Iterate over the list of currently running vms. Retrieve the core assignment for 	|
# | each VM from their configuration file. Then find the host cores that are not used	|
# | by any VM and lock the user and system processes only to those cores.               |
# |                                                                                     |
# | If config file is supplied the user must supply explicit list of allowed cores, 	|
# | this can be useful when running not only VMs in user session but also in system 	|
# | session - those will be invisible to the commands from this script.                 |
# ---------------------------------------------------------------------------------------
lock_cores(){

	MY_UID="$UID"
	
	# Define a string for locking the cores - we will add the cpu pinning for each vm in
	# next steps
	ISOLATION_STRING=""
        
	# First, get a list of all running vms
	ACTIVE_VMS=$(virsh list | tail -n +3)

	# Replace all spaces with commas so that the next for parses whole strings instead of single words
	ACTIVE_VMS=${ACTIVE_VMS//' '/','}
	
	# Get a full list of available cores
	AVAILABLE_CORES=$(unfold_core_list "$GLOBAL_CORE_LIST")
	
	# For each running vm, remove the cores from the available cores list, rewriting a list after
	# each substraction
	for VM in $ACTIVE_VMS; do
	
		# Get a name of the VM - use a cleanup function since the virsh list output is inconsistent
		# and depends on the names of running VMs
		VM_ACTIVE_NAME=$(cleanup_virsh_list "$VM")
		VM_ACTIVE_NAME=$(echo "$VM_ACTIVE_NAME" | cut -d',' -f3)

		# Check if the current VM has the core locking and if yes, reconstruct the list of locked cores
		VM_LOCKING_ALLOWED_FROM_FILE=$(cat "$TEMP_DIR"/"$VM_ACTIVE_NAME".isol)
		if [ "$VM_LOCKING_ALLOWED_FROM_FILE" == "ISOLATE" ]; then
			echo "ECHO: Isolating the cores for VM $VM_ACTIVE_NAME"
		
			VM_CORELIST=$(virsh dumpxml "$VM_ACTIVE_NAME" | grep cpupin | cut -d"'" -f4)		

			# This list will hold the info about pinned cores that will be read out from the vm config
			VM_ISOLATION_LIST=""
			# Run over the list of cores pinned to the vm
			for VM_CORE in $VM_CORELIST; do
				# Create a temporary list excluding the cores used by the VM
				NEW_CORELIST=""			
				
				# Reconstruct the list of available cores, excluding the ones pinned to the VM
				for TEST_CORE in $(echo "$AVAILABLE_CORES" | tr "," "\n"); do
					# In the new corelist include only cores that are not present in the VM config
					if [ ! "$TEST_CORE" == "$VM_CORE" ]; then
						if [ "$NEW_CORELIST" == "" ]; then
							NEW_CORELIST="$TEST_CORE"
						else
							NEW_CORELIST="$NEW_CORELIST,$TEST_CORE"
						fi
					else
						if [ "$VM_ISOLATION_LIST" == "" ]; then
							VM_ISOLATION_LIST="$TEST_CORE"
						else
							VM_ISOLATION_LIST="$VM_ISOLATION_LIST,$TEST_CORE"
						fi				
					fi
				done			
				AVAILABLE_CORES="$NEW_CORELIST"
			done
			
			# Finally add the cpu isolation command to the full command list
			SINGLE_STRING="systemctl set-property --runtime -- user-$MY_UID-$VM_ACTIVE_NAME.slice AllowedCPUs=$VM_ISOLATION_LIST"
			
			if [ "$ISOLATION_STRING" == "" ]; then
				ISOLATION_STRING="$SINGLE_STRING"
			else
				ISOLATION_STRING="$ISOLATION_STRING && $SINGLE_STRING"
			fi
		elif [ "$VM_LOCKING_ALLOWED_FROM_FILE" == "SKIP" ]; then
			echo "WARNING: Skipping the core isolation for VM $VM_ACTIVE_NAME"
		else
			echo "ERROR: Something wrong with the script (but this should never happen). Exiting"
			exit 30
		fi
	done
	
	# Finally add the last piece of isolation - isolate the rest of the processes to run only
	# on the free cores
	ISOLATION_STRING="$ISOLATION_STRING && \
	systemctl set-property --runtime -- user-${MY_UID}.slice AllowedCPUs=$AVAILABLE_CORES && \
    	systemctl set-property --runtime -- system.slice AllowedCPUs=$AVAILABLE_CORES && \
    	systemctl set-property --runtime -- init.scope AllowedCPUs=$AVAILABLE_CORES"

	# And now execute the core isolation command
	set -xe	
	sudo sh -c "$ISOLATION_STRING"
	echo "$ISOLATION_STRING"
	set +xe
}

# ---------------------------------------------------------------------------------------
# | Restore all	cores allowing them to be used by the user processes. This is required	|
# | when launching a new VM, since when the user processes are locked out of all cores	|
# | VMs fail to start.                                                                  |
# ---------------------------------------------------------------------------------------
unlock_cores(){
	set -xe
	MY_UID="$UID"
	sudo sh -c "systemctl set-property --runtime -- user-${MY_UID}.slice AllowedCPUs=$GLOBAL_CORE_LIST && \
    	systemctl set-property --runtime -- system.slice AllowedCPUs=$GLOBAL_CORE_LIST && \
    	systemctl set-property --runtime -- init.scope AllowedCPUs=$GLOBAL_CORE_LIST"
	set +ex
}

# ---------------------------------------------------------------------------------------
# | Start a virtual machine and ensure that it is locked and isolated to the specific	|
# | cpu cores according to the vm configuration.                                        |
# ---------------------------------------------------------------------------------------
start_vm(){
	NAME=$1
	
	VM_NAME=$(vm_get_name "$NAME")
	VM_CORE_ISOLATION_CONFIG=$(vm_get_isolation "$NAME")
		
	if [ "$VM_NAME" == "" ]; then
		echo "VM with alias $NAME not found"
		exit 1
	fi
	
	VM_STATUS=$(is_vm_running "$VM_NAME")

	if [ "$VM_STATUS" == "running" ]; then
		if [ "$CONFIG" == "DEFAULTS" ]; then
			echo "Error: VM $VM_NAME is already running"	
		else
			echo "Error: VM $VM_NAME aliased as $NAME is already running"	
		fi	
		exit 1
	elif [ "$VM_STATUS" == "not_found" ]; then
		if [ "$CONFIG" == "DEFAULTS" ]; then
			echo "Error: VM $VM_NAME not found in the VM list"	
		else
			echo "Error: VM $VM_NAME aliased as $NAME not found in the VM list"	
		fi	
		exit 1		
	fi

	# Get a list of current PIDs
	CURRENT_PIDS=$(pidof qemu-system-x86_64)
	
	# Unlock the CPU cores - this is required if for example the previous run of this VM
	# left the pinned cores locked and now we want to use them
	unlock_cores
	virsh start "$VM_NAME"

	# Separate a new PID from the previously existing PIDs
	NEW_PID=$(pidof qemu-system-x86_64)	
	for ITEM in $CURRENT_PIDS; do
		NEW_PID=${NEW_PID//$ITEM}
	done
	echo "$NEW_PID" > "$TEMP_DIR/$NAME.pid"
	
	# If the isolation was specified, isolate the VM from the rest of the system. First check
	# if the VM isolation was not disabled in the configuration file. If not, allow isolating
	# the cores only if runtime option was not disabled
	
	if [ "$VM_CORE_ISOLATION_CONFIG" == "true" ]; then
		echo "INFO: Core isolation enabled in the config file."
		if [ "$CORE_ISOLATE_RUNTIME" == "true" ]; then
			echo "INFO: Core isolation requested from command-line parameter. Isolating."
			create_cgroup "$NAME"
			move_pid_to_cgroup "$NAME" "$NEW_PID"
			echo "ISOLATE" > "$TEMP_DIR/$VM_NAME.isol"
		elif [ "$CORE_ISOLATE_RUNTIME" == "false" ]; then
			echo "WARNING: Core isolation not requested from command-line parameter. Skipping."
			echo "SKIP" > "$TEMP_DIR/$VM_NAME.isol"
		fi
	else
		echo "WARNING: Core isolation disabled in config file. Skipping"
		echo "SKIP" > "$TEMP_DIR/$VM_NAME.isol"
	fi
	
	# Restore the core lock - this function automatically checks all the running VMs so we don't
	# need to pass any arguments
	lock_cores
	
	# Detect if the config was used, and print alias only when config is active	
	if [ "$CONFIG" == "DEFAULTS" ]; then
		echo "INFO: Started VM $VM_NAME with PID $NEW_PID"	
	else
		echo "INFO: Started VM $VM_NAME aliased as $NAME with PID $NEW_PID"	
	fi	
}

# ---------------------------------------------------------------------------------------
# | Stop the virtual machine and reclaim the cores pinned to this VM                    |
# ---------------------------------------------------------------------------------------
stop_vm(){
	NAME=$1
	
	VM_NAME=$(vm_get_name "$NAME")
	
	if [ "$VM_NAME" == "" ]; then
		echo "VM with alias $NAME not found"
	exit 1
	fi
	STATUS=$(is_vm_running "$VM_NAME")
	
	if [ "$STATUS" == "shutdown" ]; then
		if [ "$CONFIG" == "DEFAULTS" ]; then
			echo "Error: VM $VM_NAME is not running"	
		else
			echo "Error: VM $VM_NAME with alias $NAME is not running"	
		fi	
		exit 1
	fi

	virsh shutdown "$VM_NAME"
	rm "${TEMP_DIR}/${NAME}.pid"
	rm "${TEMP_DIR}/${NAME}.isol"

	# Now finally unlock all the cores and then lock back the cores that are still
	# assigned to CMs
	unlock_cores
	lock_cores
	
	# Detect if the config was used, and print alias only when config is active	
	if [ "$CONFIG" == "DEFAULTS" ]; then
		echo "INFO: Stopped VM $VM_NAME with PID $NEW_PID"	
	else
		echo "INFO: Stopped VM $VM_NAME aliased as $NAME with PID $NEW_PID"	
	fi	
}

if [ "$OPERATION" == "start" ]; then
	start_vm "$VM_NAME_TOP"
elif [ "$OPERATION" == "stop" ]; then
	stop_vm "$VM_NAME_TOP"
elif [ "$OPERATION" == "status" ]; then
	VM_NAME=$(vm_get_name "$VM_NAME_TOP")
	is_vm_running "$VM_NAME"
elif [ "$OPERATION" == "unlock" ]; then
	unlock_cores
elif [ "$OPERATION" == "lock" ]; then
	lock_cores
fi
